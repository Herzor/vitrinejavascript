var script = document.createElement('script');
script.src = 'http://roberval.chaordicsystems.com/challenge/challenge.json?callback=X'
document.getElementsByTagName('head')[0].appendChild(script);
var qtdRecomendadoAtual = 3;
var qtdRecomendadoMax = 0;

function X(dados){
    var visitou = document.getElementById("visitou");
    //valores recuperados do JSON: ItemReferência
    var itemReferencia = dados.data["reference"].item;
    var itensRecomendados = dados.data["recommendation"];
    var nomeItemReferencia = itemReferencia["name"];
    var imagemItemReferencia = itemReferencia["imageName"];
    var urlItemReferencia = itemReferencia["detailUrl"];
    var precoItemReferencia = itemReferencia["price"];
    var infoItemReferencia = itemReferencia["productInfo"].paymentConditions;
    //------------
    //Cria coluna referente ao itens referência
    var tdProdutoReferencia = document.createElement('td');
    tdProdutoReferencia.setAttribute("class", "selecionavel");
    tdProdutoReferencia.setAttribute("onclick", "window.open('"+urlItemReferencia+"','mywindow')");
    var imagemProdutoReferencia = document.createElement('img');
    imagemProdutoReferencia.src = imagemItemReferencia;
    tdProdutoReferencia.appendChild(imagemProdutoReferencia);
    tdProdutoReferencia.appendChild(document.createElement("br"));
    var spamNomeReferencia = document.createElement('spam');
    spamNomeReferencia.setAttribute("class", "nome");
    tdProdutoReferencia.setAttribute("title", nomeItemReferencia);
    spamNomeReferencia.innerHTML = nomeItemReferencia;
    tdProdutoReferencia.appendChild(spamNomeReferencia);
    tdProdutoReferencia.appendChild(document.createElement("br"));
    var spamValorReferencia = document.createElement('spam');
    spamValorReferencia.setAttribute("class", "valor");
    spamValorReferencia.innerHTML = "Por: <b>"+precoItemReferencia+"</b>";
    tdProdutoReferencia.appendChild(spamValorReferencia);
    tdProdutoReferencia.appendChild(document.createElement("br"));
    var spamCondicoesReferencia = document.createElement('spam');
    spamCondicoesReferencia.setAttribute("class", "condicoes");
    spamCondicoesReferencia.innerHTML = infoItemReferencia+" sem juros";
    tdProdutoReferencia.appendChild(spamCondicoesReferencia);
    visitou.appendChild(tdProdutoReferencia);

    for (contador in itensRecomendados) {
        var vitrine = document.getElementById("vitrine");
        //valores recuperados do JSON: ItemRecomendado
        var nomeItem = itensRecomendados[contador].name;
        var imagemItem = itensRecomendados[contador].imageName;
        var urlItem = itensRecomendados[contador].detailUrl;
        var precoItem = itensRecomendados[contador].price;
        var precoAntigoItem = itensRecomendados[contador].oldPrice;
        var infoItem = itensRecomendados[contador].productInfo["paymentConditions"];
        //------------
        //Cria colunas referentes ao itens recomendados
        var tdProduto = document.createElement('td');
        tdProduto.setAttribute("id", contador);
        tdProduto.setAttribute("class", "selecionavel");
        tdProduto.setAttribute("onclick", "window.open('"+urlItem+"','mywindow')");
        var imagemProduto = document.createElement('img');
        imagemProduto.src = imagemItem;
        tdProduto.appendChild(imagemProduto);
        tdProduto.appendChild(document.createElement("br"));
        var spamNome = document.createElement('spam');
        spamNome.setAttribute("class", "nome");
        tdProduto.setAttribute("title", nomeItem);
        spamNome.innerHTML = nomeItem;
        tdProduto.appendChild(spamNome);
        tdProduto.appendChild(document.createElement("br"));
        if(precoAntigoItem != null){
            var spamValorAntigo = document.createElement('spam');
            spamValorAntigo.setAttribute("class", "valorAntigo");
            spamValorAntigo.innerHTML = "De: "+precoAntigoItem;
            tdProduto.appendChild(spamValorAntigo);
            tdProduto.appendChild(document.createElement("br"));
        }
        var spamValor = document.createElement('spam');
        spamValor.setAttribute("class", "valor");
        spamValor.innerHTML = "Por: <b>"+precoItem+"</b>";
        tdProduto.appendChild(spamValor);
        tdProduto.appendChild(document.createElement("br"));
        var spamCondicoes = document.createElement('spam');
        spamCondicoes.setAttribute("class", "condicoes");
        spamCondicoes.innerHTML = infoItem+" sem juros";
        tdProduto.appendChild(spamCondicoes);
        vitrine.appendChild(tdProduto);
        qtdRecomendadoMax++;
        if(contador >= 4){
            document.getElementById(contador).classList.add("hidden");
        }
    }
}
//avança entre os itens recomendados
function avancarRecomendado() {
    if(qtdRecomendadoMax-1 > qtdRecomendadoAtual){
        document.getElementById(qtdRecomendadoAtual-3).classList.add("hidden");
        qtdRecomendadoAtual++;
        document.getElementById(qtdRecomendadoAtual).classList.remove("hidden");
    }

}
//retrocede entre os itens recomendados
function voltarRecomendado() {
    if(qtdRecomendadoAtual > 3){
        document.getElementById(qtdRecomendadoAtual).classList.add("hidden");
        qtdRecomendadoAtual--;
        document.getElementById(qtdRecomendadoAtual-3).classList.remove("hidden");
    }


}